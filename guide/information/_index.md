---
title: Information about the Project
chapter: true
weight: 2
menu: 
  sidebar:
    name: "Project Fundamentals"
---

# Project Fundamentals

Inkscape is a free and Open Source Vector graphics editor - a computer program; and Inkscape is also a worldwide community of contributors who work together to create and promote the software 'Inkscape'.
