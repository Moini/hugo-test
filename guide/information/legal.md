---
title: Legal
weight: 2
menu:
  sidebar:
    name: "Legal"
    parent: "Project Fundamentals"
---

## Inkscape Project Leadership Committee

## Software Freedom Conservancy

## Fiscal Sponsorship Agreement

## Membership
