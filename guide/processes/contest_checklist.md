---
title: Reviewing Entries for the Inkscape About Screen Contest
weight: 3
menu:
  sidebar:
    name: "Contest Checking"
    parent: "Processes"
---

* Make sure you're a member of the [Vectors team](https://inkscape.org/*inkscape-vectors/) (otherwise you will not be able to mark a contest entry as checked)
* Don't forget to subscribe to the gallery here: https://inkscape.org/gallery/=about-screen-contest/contest-for-14/ !
* When an item has been checked, and is compliant to the rules, let the author know in a comment …
* … and click on 'Item is checked'!

## Check these manually:
* [ ] Is the license on the website CC-BY-SA 4.0?
* [ ] Does the image contain the unedited Branding layer (either white/black)? (see https://inkscape.org/about/branding/ )
* [ ] Does the image contain the author's signature / name / online handle, but without any added social media site name / link? (e.g. 'inkscape' not '@inkscape@mastodon.art')(optional)
* [ ] Does it look like third-party materials (e.g. photos for tracing, vector clip art) were used? If so, does the submitter have permission to use it? Did they credit the owner? Permission includes: permission to redistribute, to modify, permission to sell. Non-commercial is not enough! Compatibility with CC-By-SA license is required. Public domain/CC-0, CC-By and CC-By-SA are okay. This point is important!
* [ ] Did the submitter provide names and links for all fonts that are used in the drawing in the description on the website?
* [ ] Are all fonts used in the drawing licensed under an open source license (OFL, GPL, Apache, MIT, PD/CC-0, CC-By-SA, ...)?
* [ ] Does the drawing contain all texts as an invisible, editable copy?
* [ ] Is the content of the drawing appropriate for representing the project (suitable for work and children, no politics, nudity, violence, religion)?


## Automated checks


### Using the Contest Checking Extension

* Download the [Contest Checking Extension](https://gitlab.com/joneuhauser/inkscape-about-contest-validator) and install it.
* Find it in Inkscape: `Extensions > Document > About screen contest checker`
* Open downloaded image and use the extension
* Read the output and, if necessary, check things manually or notify the image's author of required changes

This is what the extension checks for:

* [ ] Are the canvas dimensions must be 750x625 px?
* [ ] Is the background covered completely? (no transparent areas left)
* [ ] Is the license in the file metadata CC-By-SA 4.0?
* [ ] Is there an author name in the file metadata?
* [ ] Does the image contain the hallmarks of being created with Inkscape? Open the file in a text editor and check the beginning of the file or check the beginning of the file in the XML editor. Does it look similar to what is shown below this list? (and does not show signs of being created with Adobe Illustrator)
* [ ] Does the image load comparatively fast? (no heavy filtering, no myriads of objects)
* [ ] Has the user done a document cleanup?
* [ ] Are all texts converted to paths?
* [ ] Does the image not contain any raster graphics, unless for masks or patterns? (but then: properly attributed, see above)

### Checked automatically by website:

* [ ] Did the submitter at a maximum submit one more entry to the contest (2 in whole)?


## Example of a document start of a document that was created with Inkscape:

```
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   sodipodi:docname="some_file_name.svg"
   inkscape:version="1.1.1 (95f46a2859, 2021-12-26)"
```