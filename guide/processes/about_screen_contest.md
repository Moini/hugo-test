---
title: Running an About Screen Contest
weight: 2
menu:
  sidebar:
    name: "About Screen Contest"
    parent: "Processes"
---

**Table of Contents**
{{% toc %}}


## General Info

### Goals

Finding a new About Screen artwork for a new Inkscape version, and:

* encouraging testing the new version
* promoting Inkscape in general and the new version with its features
* generating artwork that can be used in other areas of the project

### Teams involved
{{<mermaid align="left">}}
%%{init: {'theme':'forest', 'flowchart':{'nodeSpacing': -20}, 'themeVariables': {'fontSize': '30px'}}}%%
flowchart TD
    id1[/Vectors/]
    id2[/Website/]
    id3[/Developers/]
{{</mermaid>}}


### A Three-Step-Process

1. Test alpha version + collect material for outreach through **Contest submissions**
2. Get community excited about new version through **Community voting**
3. Give contributors a perk and some appreciation, update members list and filter out unsuitable contest finalists through **Contributor voting**

### Timeline

* Day 0 (Sunday): First day of Submission period
* \+ 6 weeks (Sunday): Last day of Submission period
* \+ 1 day (Monday): First day of Community Voting
* \+ 6 days (Sunday): Last day of Community Voting
* \+ 3 days (Wednesday): First day of Contributor Voting
* \+ 1.5 weeks (Sunday): Last day of Contributor voting
* \+ 2 days (Tuesday): Winner announcement

## Tasks

### General

* Make sure to always keep team in the loop about current focus
* Coordinate with developers about release dates (alpha, beta, final)
* Find example content from previous contests at https://mastodon.art/@inkscape, https://inkscape.org/news/ , https://lists.inkscape.org/postorius/lists/ , https://gitlab.com/inkscape/vectors/content/-/issues/108 )
* When announcing times, remember that website time is UTC (also remember daylight savings switch in spring!)
* News articles and social media posts should always contain a relevant image.
* Whenever a news article is published, translation team needs to be notified.
* Make use of all relevant channels for publicity (chat, mailing lists, social media, news articles), even if not detailed in the following (I tried my best...)
* Keep the website content up-to-date with current status of the contest at all times
* Licensing is not a minor point. Inkscape can not ship with a graphic that violates anyone's copyrights. Make doubly sure that everyone knows that, and that submissions do not include / derive from any third-party content that does not use an open source license.

### Before the Contest Starts:
1. Decide on the following with the team:
    1. Timeline
    2. Theme/Topic (previous vote: https://office.inkscape.org/nextcloud/index.php/apps/polls/s/FEqgwxWDCCcxY8BT , maybe just use 2nd from last time for 1.4?)
    3. Template (yes/no), prepare one, if needed - 1.3 contest has shown that a fixed template has drawbacks, consider dropping this and replacing with previous 1.2 contest rules about branding inclusion!
    4. Rule review (e.g. for 1.4 contest: add AI exclusion!)
2. Prepare website:
    1. Create contest gallery (example: https://inkscape.org/gallery/=about-screen-contest/contest-for-13/) with
        * Nice description text with all relevant information / links
        * Image
        * Correct settings for timeline, license etc. (compare example gallery)
    2. Update [About Screen Web page](https://inkscape.org/en/community/about-screen-contests/)
        * Times
        * Topic
        * Rules according to review results
        * Adjust any links
        * Adjust text to current contest phase
    3. Create news article about Contest start, timeline, topic (example: https://inkscape.org/news/2023/02/17/13-about-screen-contest-celebrating-20-years-of-in/)
    4. Create social media posts for Inkscape's channels about contest
    5. Create an email for Inkscape's mailing lists (users, developers, board, vectors, translators, announce, docs)
    6. Collect a team of reviewers, introduce them to the tools and requirements of the contest 
        * Make sure that the [contest check extension](https://gitlab.com/inkscape/extras/inkscape-about-contest-validatorhttps://gitlab.com/inkscape/extras/inkscape-about-contest-validator) works with current Inkscape version (if not, ask extension author for a fix)
        * Make sure reviewers have it installed and know how to use it
        * Educate reviewer team about:
            * Licensing
            * Raster images
            * Font usage
            * Other contest rules (branding, template, allowed topics, [community guidelines](https://inkscape.org/community/coc/) ...)
            * Let them know about [the contest checklist]({{< ref "contest_checklist.md" >}})
        * Investigate team's availability and preferences, so review tasks can be shared

### When Contest Starts

* Update [Website top banner](https://inkscape.org/admin/el_menu/menuitem/)
* Test gallery by submitting an item (and deleting it afterwards) - any bugs need to be reported to Martin Owens / [the website issue tracker](https://gitlab.com/inkscape/inkscape-web/-/issues) / [the website chat](https://chat.inkscape.org/channel/team_website) ASAP
* Publish news article (coordinate with Alpha release!)
* Post emails to mailing lists
* Publish Social media posts
* Post info on the chats, too (esp. offtopic, user, devel)

### While Contest Runs

* Daily:
    * Check for new submissions and review them for conformance to contest rules
    * Hide gross/fake/joke entries
    * Leave comments to authors with requests for fixing
    * If fixed, give entries the 'checked' status & notify author in a comment that they're a part of the contest now
    * Review entries where the team has asked for a fix and check whether they have been fixed
    * Invite people who have difficulties fixing their entry to the forum and help them there
    * Check back with reviewer team about questions / difficulties
* Weekly: 
    * Remind community in social media posts / chat about ongoing contest, show previews and statistics of submitted items, make sure everyone understands that they may still need to fix things before their entries can be admitted to the contest
* Once:
    * Send out reminder email to mailing lists, too
* Already prepare news article skeleton for finalists announcement
* Set up team meeting for day before contest end!
* In that meeting:
    * Consider extending the deadline in the team, if not enough good submissions, but enough time remaining until release (ask developers). If extension:
        * Notify all channels (social, mailing list, chat)
        * Adjust times on Contest page
        * Adjust times for Contest gallery
    * Talk about entries / difficulties / license issues
    * Plan article / image / social posts

### After Contest Submissions are Closed

* Re-check all entries that aren't checked
* Allow artists to make some last fixes and submit those fixes through the admin interface (editing is disallowed)
* Hide any remaining joke/spam entries
* Prepare Community Voting:
    * Update Contest web page
    * Update website banner
    * Write news article about Community voting (example: https://inkscape.org/news/2022/03/07/vote-your-favourite-inkscape-about-screen-inkscape/), coordinate with beta release and release documentation, so maybe beta release can be announced in the same article
    * Prepare social media posts for both community voting & beta release

### When Community Voting Starts

* Announce on social media, chat and mailing lists
* Update website banner
* Update Contest web page
* Update Contest gallery description
* If applicable, also announce beta release on social & call for testers
* Monitor voting for bot votes, and weird patterns

### When Community Voting Ended / Contributor Voting Starts

* Announce finalists on social media (possibly in news, too)
* Update Contest web page
* Update website banner
* Update contest gallery description
* Add banners to 5 Finalists' entries in gallery ('Next Round')
* Create new gallery on the website for contributor voting, with only the finalists:
    * Upload images yourself
    * Include descriptions
    * Assign to authors or at least put them into the 'By' field
    * Move them into a newly created gallery with an appropriate name
    * Set up voting dates, set vote type to ranked choices
    * Set gallery owning group: Contest organizers, gallery voting group: Inkscape members
    * Edit gallery description and image for Contributor voting
    * If assigned: Notify authors of duplicates and that they are to leave those alone (that way, votes are not confused with previous community voting)
    * example: see https://inkscape.org/gallery/=artwork/inkscape-13-about-screen-contributor-voting/
* Invite contributors with voting rights to vote and invite contributors who are not a member yet to [apply for membership](https://inkscape.org/*membership/) (mailing lists + chat, potentially also make use of members notification feature on website)

### During Contributor Voting

* Daily:
    * Ensure that membership requests are attended to and confirmed by other members in a timely manner, so everyone who wants to vote and should be able to vote is able to do so.
* Twice:
    * Send out reminders to all channels where contributors are found (chat, mailing lists)

### Winner Announcement

1. Reach out to winner in a comment and congratulate them
2. Reach out to other finalists, and thank them
3. Update Contest web page
4. Add little banners to winner images ('Winner', 'Runner Up') in Contributor voting gallery and in Community voting gallery
5. Consider updating Contributor voting gallery description
6. Update website banner
7. Create short news article with image about Contest being done, include statistics, interesting tidbits, thank everyone involved etc. (example: https://inkscape.org/news/2022/03/22/new-discoveries-await-upcoming-inkscape-12-about/)
8. Create social media posts with image and alt text (example: https://mastodon.art/@inkscape/110177387850021075 )
9. Update [Contest winner gallery for the website](https://inkscape.org/gallery/=inkscape-branding/inkscape-about-screens/) with the new image
10. Ensure that some developer includes the new image in the Inkscape code
11. Update website hero image (all languages!)
12. Use winner image for release / beta release in releases app.

### Winner Interview

1. Reach out to winner by email asking whether they'd be willing to be interviewed. Include links previous interviews and include interview questions (examples: https://inkscape.org/news/2023/05/23/meet-inkscapes-new-about-screen-artist/ , ).
2. Add image, fix grammar / typos, and turn it into a news article
3. Publish
4. Create social media posts and publish.

### After Contest End

* Meet up with team for a review
* Consider update to rules etc.
* Give a huge sigh of relief and a huge THANK YOU to everyone involved
* Enjoy new Inkscape release with an image that you contributed to

