---
title: Meetings
weight: 1
menu:
  sidebar:
    name: "Meetings"
    parent: "Processes"
---

* [ ] Announce meeting
* [ ] Hold meeting
* [ ] Create meeting notes
* [ ] Publish meeting notes
* [ ] Announce published meeting notes
